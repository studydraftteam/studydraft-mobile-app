#StudyDraft App

The mobile application for StudyDraft.

Built using the Ionic/Angular/Cordova framework.

##Up and Running

Clone the repository
> git clone http://github.com/alexklibisz/studydraft-mobile-app  
> cd studydraft-mobile-app  

Install the packages
> npm install -g cordova ionic  
> npm install  

Install the ionic platforms and plugins  
> ionic state reset  

###Android Configuration

1. Install java. On the ubuntu the command is <code>sudo apt-get install openjdk-8-jdk</code>.
2. Download the appropriate SDK tools [from this page](https://developer.android.com/sdk/index.html#Other)  
2. Extract the directory to a permanent location - i.e. ~/.software/android-sdk
3. Open a terminal and cd into the tools directory, then run <code>./android</code>
4. A window will open called *Android SDK Manager*. Towards the bottom it says *Select New or Updates*. Click *New*, then click *Install n packages*. Make sure the Android Support Library and Android Support Repository are both 
5. Accept the license and install the packages. This will take some time.
6. From now on you should be able to run the Android emulator: <code>ionic build android && ionic emulate android</code> and run the app on a connected device: <code>ionic build android && ionic run android</code>

####Common Problems

- CordovaPush library is not behaving correctly:
  -  Make sure the Android Support Library and Android SUpport Repository SDK packages are installed.

##Conventions

###Minification Proof
- When declaring controllers, factories, directives, services, etc: include the dependency injections as string literals for minification purposes. (re: http://thegreenpizza.github.io/2013/05/25/building-minification-safe-angular.js-applications/).

###Separation of presentation behavior and application behavior
- Try as much as possible to separate presentational behavior ($location redirects, popups, alert messages, etc) from application behavior (authentication, API calls, etc.).
- Presentational behavior should only be executed from the controller and up. Don't do that stuff in the services/factories, because their presentational behavior will change depending on context.
- For example, the AuthFactory.checkUser() function returns true/false and does nothing else. If a redirect should happen if this function is successful, then this is done at the level where the function was called.

##Configurations

###Constant Values for Configuration
- Constants should be defined in app.js as <code>studydraft.constant('config', { /* here */ })</code>.
- Constants should be injected into controllers, factories, etc. like other dependencies.
- Constants should follow ALL_CAPS_UNDERSCORE naming.

###Authentication Scheme
- Authentication and route protection scheme is similar to this: http://stackoverflow.com/questions/20969835/angularjs-login-and-authentication-in-each-route-and-controller

###Installing Sass, Bourbon, etc.
```
gem install sass
gem install bourbon
gem install neat

cd /css/libs/directory
bourbon install
neat install
```
This will install bourbon in the directory 'bourbon'. The same goes for neat.
