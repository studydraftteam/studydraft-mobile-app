var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var inject = require('gulp-inject');
var eslint = require('gulp-eslint');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');

var argv = require('minimist')(process.argv.slice(2));

/**
 * Define the paths for sass, js, css, html assets.
 */
var paths = {
  sass: ['www/app/**/*.scss'],
  js: ['www/app/**/*.js'],
  css: ['www/app/**/*.css'],
  html: ['www/app/**/*.html']
};

gulp.task('config', function() {
  return gulp.src('./www/config/app.config.' + (argv['env'] || 'local') + '.js')
    .pipe(rename('app.config.js'))
    .pipe(gulp.dest('./www/app'));
});

gulp.task('default', ['sass', 'eslint', 'inject', 'watch']);

gulp.task('eslint', function() {
  return gulp.src(paths.js)
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

/**
 * Inject scripts and styles into index.html
 */
gulp.task('inject', function() {

  var indexFile = './www/index.html',
    injectedFiles = ['./www/app/app.js', './www/app/**/*.js', './www/app/**/*.css'];

  return gulp.src(indexFile).pipe(inject(gulp.src(injectedFiles, {
      read: false
    }), {
      relative: true
    }))
    .pipe(gulp.dest('./www'));

})

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

/**
 * Preprocess and minify all .scss stylesheets
 * into their equivalent .css and .min.css
 */
gulp.task('sass', function(done) {
  gulp.src(paths.sass, {
      base: './'
    })
    .pipe(sourcemaps.init())
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.js, ['eslint', 'inject']);
  gulp.watch(paths.css, ['inject']);
});
