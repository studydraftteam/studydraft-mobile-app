/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function() {
  'use strict';

  studydraft.controller('MyAccountController', ['AuthService', 'UserService', 'UniversityService',
    '$ionicModal', '$ionicPopup', '$ionicLoading', '$location', '$rootScope', '$scope', '$window',
    MyAccountController
  ]);

  function MyAccountController(AuthService, UserService, UniversityService, $ionicModal,
    $ionicPopup, $ionicLoading, $location, $rootScope, $scope, $window) {

    $scope.settingsForm = new SettingsForm();
    $scope.changePasswordForm = new ChangePasswordForm();
    $scope.selectUniversityModal = null;
    $scope.changePasswordModal = null;
    $scope.syncFacebook = syncFacebook;
    $scope.universityLimit = 30;

    // Define the university modal
    $ionicModal.fromTemplateUrl('app/my-account/my-account-select-university.modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
      init: function() {
        $scope.selectUniversityForm = new SelectUniversityForm();
      }
    }).then(function(modal) {
      $scope.selectUniversityModal = modal;
    });

    // Define the password change modal
    $ionicModal.fromTemplateUrl('app/my-account/my-account-change-password.modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.changePasswordModal = modal;
    });

    $scope.loadMore = function() {
      $scope.universityLimit = 30 + $scope.universityLimit;
    };

    /**
     * Object containing data and logic used for listing
     * and modifying a user's attributes.
     * @constructor
     */
    function SettingsForm() {
      var self = this;

      AuthService.checkUser().then(function() {
        UserService.getUser(AuthService.currentUser.id, 'university').then(function(user) {
          self.user = user;
        });
      });

      self.submit = function() {
        UserService.saveUser(self.user).then(function(result) {
          AuthService.currentUser = result;
          self.user = result;
          $ionicPopup.alert({
            title: 'Changes Saved',
            template: '<center>Thank you ' + self.user.name + '</center>'
          });
          $scope.settingsForm = new SettingsForm();
        })
        .catch(function(error) {
          $ionicPopup.alert({
            title: 'Save Failed',
            template: '<center>' + error.summary + '</center>'
          });
        });
      };
    }

    /**
     * Object containing data and logic used for listing
     * and adding universities to a user.
     *
     * Submit function calls the settingsForm submit function.
     * @constructor
     */
    function SelectUniversityForm() {
      var self = this;
      // The radio-select model binds to
      // the selectedUniversity
      self.selectedUniversity =
        $scope.settingsForm.user.university;

      // Get and populate the full list of universities
      self.universities = [];
      $scope.showLoading();
      UniversityService.getUniversityList().then(function(result) {
        self.universities = result;
        $scope.hideLoading();
      });

      self.submit = function() {
        // Set the user's university to the one that was selected
        $scope.settingsForm.user.university =
          self.selectedUniversity.id;
        $scope.settingsForm.submit();
      };
    }

    function ChangePasswordForm() {
      var self = this;
      self.currentPassword = '';
      self.newPassword = '';
      self.newPasswordConfirm = '';

      self.submit = function() {
        UserService.changePassword(AuthService.currentUser.id, self.currentPassword, self.newPassword, self.newPasswordConfirm)
          .then(function() {
            $scope.changePasswordModal.hide();
            return $ionicPopup.alert({
              title: 'Password has been updated',
              template: '<center>You will be logged out. Log back in with your new password.</center>'
            });
          })
          .then(function() {
            return AuthService.logout();
          })
          .then(function() {
            $window.location.reload();
          })
          .catch(function(response) {
            $ionicPopup.alert({
              title: 'Password update failed',
              template: '<center>' + response.data.summary + '</center>'
            });
          });
      };
    }

    function syncFacebook() {
      // Execute the Facebook login sequence.
      // Re-initialize the settings form on completion.
      UserService.syncFacebook(AuthService.currentUser.id)
        .then(function() {
          $ionicPopup.alert({
            title: 'Account has been linked to Facebook',
            template: '<center>Thank you ' + AuthService.currentUser.username + '</center>'
          });
          $scope.settingsForm = new SettingsForm();
        })
        .catch(function(error) {
          $ionicPopup.alert({
            title: 'Something went wrong',
            template: '<center>' + error.summary + '. Try again or contact support@studydraftapp.com.</center>'
          });
        });
    }

    $scope.showLoading = function() {
      $ionicLoading.show({
        template: 'Loading...'
      });
    };

    $scope.hideLoading = function() {
      $ionicLoading.hide();
    };

  }


})();
