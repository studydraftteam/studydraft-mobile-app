/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 7/13/15
 */
(function() {
  'use strict';
  studydraft.controller('ProfileController', ['$window', '$ionicPopup', '$scope', '$stateParams', 'AuthService',
    'UserService', 'UniversityService', 'isMyProfile', ProfileController
  ]);

  function ProfileController($window, $ionicPopup, $scope,
    $stateParams, AuthService, UserService, UniversityService, isMyProfile) {

    $scope.user = {};
    $scope.activeTab = 'courses';
    $scope.openEmail = openEmail;
    $scope.openFacebook = openFacebook;
    $scope.openSms = openSms;
    $scope.openCall = openCall;
    
    $scope.userId = isMyProfile ? AuthService.currentUser.id : $stateParams.id;

    // Get User with university and courses
    UserService.getUser($scope.userId, 'university,courses')
      .then(function(data) {
        $scope.user = data;
      })
      .catch(function(error) {
        $ionicPopup.alert({
          title: 'Something went wrong',
          template: '<center>' + error.summary + '</center>'
        });
      });

    function openEmail() {
      $window.location.href = 'mailto:' + $scope.user.email + '?subject=StudyDraft';
    }

    function openFacebook() {
      if ($scope.user.facebookId > 0) {
        // Alternative: CordovaEmailComposer plugin
        $window.open('https://www.facebook.com/' + $scope.user.facebookId, '_system');
      } else {
        $ionicPopup.alert({
          title: 'User\'s Facebook is Not Available',
          template: '<center>This user has either not yet added his/her Facebook or has made it private.'
        });
      }
    }

    function openSms() {
      if ($scope.user.phoneNumber > 0) {
        // Alternative: CordovaSMS plugin
        $window.location.href = 'sms:' + $scope.user.phoneNumber;
      } else {
        $ionicPopup.alert({
          title: 'User\'s Phone Number is Not Available',
          template: '<center>This user has either not yet added his/her phone number or has made it private.'
        });
      }
    }

    function openCall() {
      if($scope.user.phoneNumber > 0) {
        $window.location.href = 'tel:' + $scope.user.phoneNumber;
      } else {
        $ionicPopup.alert({
          title: 'User\'s Phone Number is Not Available',
          template: '<center>This user has either not yet added his/her phone number or has made it private.'
        });
      }
    }

  }

})();
