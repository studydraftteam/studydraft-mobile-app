/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function() {
  'use strict';

  studydraft.controller('AuthLoginController', ['AuthService', '$ionicModal',
    '$ionicPopup', '$location', '$scope', '$log', 'UserService', AuthLoginController
  ]);

  function AuthLoginController(AuthService, $ionicModal, $ionicPopup,
    $location, $scope, $log, UserService) {

    $scope.loginForm = {
      email: '',
      password: '',
      /**
       * Execute the login function with the given email/password.
       * Execute the checkLogin function.
       * @return {null} nothing
       */
      submit: function() {
        var form = $scope.loginForm;

        AuthService.login({
            email: form.email,
            password: form.password
          })
          .then(loginSuccessful)
          .catch(loginError);
      }
    };

    $scope.passwordResetForm = {
      email: '',
      submit: function() {
        var form = $scope.passwordResetForm;
        UserService.resetPassword(form.email).then(passwordResetSuccessful, passwordResetError);
      }
    };

    $ionicModal.fromTemplateUrl('password-reset-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.passwordResetModal = modal;
    });

    /**
     * Renders the Facebook login popup that handles Facebook authentication.
     */
    $scope.loginFacebook = function() {
      AuthService.loginFacebook()
        .then(loginSuccessful)
        .catch(loginError);
    };

    function loginSuccessful() {
      $location.path('/');
    }

    function loginError(response) {
      $log.error(response);
      $ionicPopup.alert({
        title: 'Login Error',
        template: '<center>' + response.data.summary + '</center>'
      });
    }

    function passwordResetSuccessful() {
      $ionicPopup.alert({
        title: 'Password Reset Successful',
        template: '<center>Check your email for a temporary password.</center>'
      });
      $scope.passwordResetModal.hide();
    }

    function passwordResetError() {
      $ionicPopup.alert({
        title: 'Password Reset Failed',
        template: '<center>Please try again or contact support@studydraftapp.com</center>'
      });
    }
  }
})();
