/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function() {
  'use strict';

  studydraft.controller('AuthRegisterController', ['$ionicPopup', '$location', '$scope', 'AuthService', AuthRegisterController]);

  function AuthRegisterController($ionicPopup, $location, $scope, AuthService) {

    $scope.registerForm = new RegisterForm();
    $scope.registerFacebook = registerFacebook;

    function RegisterForm() {
      var self = this;
      self.firstName = '';
      self.lastName = '';
      self.password = '';
      self.phoneNumber = '';
      self.email = '';
      self.submit = function() {
        AuthService.register({
            firstName: self.firstName,
            lastName: self.lastName,
            password: self.password,
            phoneNumber: self.phoneNumber,
            email: self.email
          })
          .then(registerSuccessful)
          .catch(registerError);
      };
    }

    function registerFacebook() {
      AuthService.registerFacebook()
        .then(registerSuccessful, registerError);
    }

    function registerSuccessful() {
      $ionicPopup.alert({
        title: 'You\'ve been registered!',
        template: '<center>Click OK to return to the login screen.</center>'
      }).then(function() {
        $location.path('/');
      });
    }

    function registerError(response) {
      $log.log(response);
      $ionicPopup.alert({
        title: 'An error occurred.',
        template: '<center>' + response.data.summary + ' Please try again.</center>'
      });
    }

  }

})();
