/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/8/2014
 */
(function() {
  'use strict';

  studydraft.controller('MyCoursesController', ['AuthService', 'CourseService',
    '$ionicLoading', '$ionicModal', '$ionicPopup', '$rootScope', '$scope', 'UniversityService',
    'UserService', MyCoursesController
  ]);

  function MyCoursesController(AuthService, CourseService, $ionicLoading, $ionicModal,
    $ionicPopup, $rootScope, $scope, UniversityService, UserService) {

    $scope.$on('CURRENT_USER_COURSES', function(event, params) {
      if(params.reload) {
        controllerInit();
      }
    });

    // Define scope variables and functions
    $scope.semester = 'Fall 2015';
    $scope.courses = [];
    $scope.auth = AuthService;

    function controllerInit() {
      // Define the Add Course Modal
      $ionicModal.fromTemplateUrl('app/my-courses/my-courses-add-course.modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        init: function() {
          $scope.addCourseForm = new AddCourseForm();
        }
      }).then(function(modal) {
        $scope.addCourseModal = modal;
      });

      // Define the Create Course Modal
      $ionicModal.fromTemplateUrl('app/my-courses/my-courses-create-course.modal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        init: function() {
          $scope.createCourseForm = new CreateCourseForm();
        }
      }).then(function(modal) {
        $scope.createCourseModal = modal;
      });

      // Get the user's courses
      AuthService.checkUser()
        .then(function() {
          UserService.getCourses(AuthService.currentUser.id)
            .then(function(data) {
              $scope.courses = data;
            });
        });
    }
    controllerInit();

    /**
     * Object containing data and logic used for listing
     * and adding a new class to a user.
     * Used in the AddClassModal.
     */
    function AddCourseForm() {
      var self = this;
      self.courses = [];
      self.submit = submit;

      // Get courses at the user's university
      $ionicLoading.show({
        template: 'Loading...'
      });
      UniversityService.getCourses(AuthService.currentUser.university)
        .then(function(courses) {
          self.courses = courses;
          $ionicLoading.hide();
        });

      function submit() {

        var courseIds = [];
        self.courses.forEach(function(course) {
          if (course.selected) {
            courseIds.push(course.id);
          }
        });

        // Associate the courses with the user
        // and persist to the API
        UserService.associate(AuthService.currentUser.id, 'courses', courseIds)
          .then(function() {
            $ionicPopup.alert({
              title: 'Changes Saved',
              template: ''
            });
            $scope.addCourseModal.hide();
            controllerInit();
          }, function(error) {
            $ionicPopup.alert({
              title: 'Save Failed',
              template: '<center>' + error.data.summary + '</center>'
            });
          });

      }
    }

    /**
     * Object containing form setup and logic used for
     * creating a new class.
     * Used in the CreateClassModal.
     */
    function CreateCourseForm() {
      var self = this;
      self.course = {};
      self.course.university = AuthService.currentUser.university;
      self.course.term = 'Fall';
      self.course.year = new Date().getFullYear();
      self.submit = submit;

      function submit() {
        CourseService.createCourse(self.course)
          .then(function(result) {
            result.data.selected = true;
            $scope.addCourseForm.courses.push(result.data);
            $scope.addCourseForm.submit();
            $scope.createCourseModal.hide();
          })
          .catch(function(error) {
            $ionicPopup.alert({
              title: 'Save Failed',
              template: '<center>' + error.data.summary + '</center>'
            });
          });
      }
    }

  }

})();
