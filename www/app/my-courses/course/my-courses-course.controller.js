(function() {
  'use strict';

  studydraft.controller('MyCoursesCourseController', ['AuthService', 'CourseService', '$ionicPopover', '$rootScope', '$scope', '$state', '$stateParams', 'UserService', MyCoursesCourseController]);

  function MyCoursesCourseController(AuthService, CourseService, $ionicPopover, $rootScope, $scope, $state, $stateParams, UserService) {

    $scope.course = {};
    $scope.viewTitle = $stateParams.shortName;
    $scope.activeTab = 'members';
    $scope.leaveCourse = leaveCourse;

    $ionicPopover.fromTemplateUrl('options-popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.optionsPopover = popover;
    });

    CourseService.getCourse($stateParams.id, 'users,studySessions')
      .then(function(data) {
        $scope.course = data;
      });

    function leaveCourse() {
      UserService.disassociate(AuthService.currentUser.id, 'courses', [$scope.course.id])
        .then(function() {

          $scope.optionsPopover.hide();

          $state.go('base.tab.mycourses')
            .then(function() {
              $rootScope.$broadcast('CURRENT_USER_COURSES', {
                reload: true
              });
            });
        })
        .catch(function(error) {
          $log.error(error);
        });
    }
  }
})();
