var config = {
  /**
   * The base for endpoints that will be hit
   * @type {String}
   */
  API_URL: '/api',
  /**
   * Facebook App Id defined in the FB developer console
   * @type {String}
   */
  FB_APP_ID: '1022390854446394',
  /**
   * The permissions with which the user should authorize Facebook access.
   * @type {Array}
   */
  FB_PERMISSIONS: ['public_profile', 'user_friends', 'email'],
  /**
   * The google project number shown on the project overview page
   * @type {String}
   */
  GOOGLE_PROJECT_NUMBER: '668544663629'
};

studydraft.constant('config', config);
