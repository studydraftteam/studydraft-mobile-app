/**
 * Routing Configuration
 */
studydraft.config(function($stateProvider, $urlRouterProvider) {
  'use strict';
  $stateProvider

  /**
   * Define the 'base' abstract state.
   * This is the parent of all other states and abstract states.
   * The template for this state contains the global header and
   * side menu.
   */
    .state('base', {
    abstract: true,
    url: '',
    templateUrl: 'app/common/views/base.abstract.html'
  })

  .state('base.myprofile', {
    url: '/myprofile',
    templateUrl: 'app/profile/profile.view.html',
    controller: 'ProfileController',
    resolve: {
      isMyProfile: function() {
        return true;
      }
    }
  })

  .state('base.myaccount', {
    url: '/myaccount',
    templateUrl: 'app/my-account/my-account.view.html',
    controller: 'MyAccountController'
  })

  .state('base.settings', {
    url: '/settings',
    templateUrl: 'app/settings/settings.view.html',
    controller: 'SettingsController'
  })

  /**
   * Define the 'base.tab' abstract state.
   * This is a child of the base abstract state and the parent
   * of all tab states.
   */
  .state('base.tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'app/common/views/tabs.abstract.html'
  })

  .state('base.tab.mycourses', {
    url: '/mycourses',
    views: {
      'MyCourses': {
        templateUrl: 'app/my-courses/my-courses.view.html',
        controller: 'MyCoursesController'
      },
      cache: false
    }
  })

  .state('base.tab.mycourses.course', {
    url: '/course/:id/:shortName',
    views: {
      'MyCourses@base.tab': {
        templateUrl: 'app/my-courses/course/my-courses-course.view.html',
        controller: 'MyCoursesCourseController'
      }
    }
  })

  .state('base.tab.mycourses.course.profile', {
    url: '/profile/:id',
    views: {
      'MyCourses@base.tab': {
        templateUrl: 'app/profile/profile.view.html',
        controller: 'ProfileController'
      }
    },
    resolve: {
      isMyProfile: function() {
        return false;
      }
    }
  })


  .state('base.tab.studyboard', {
    url: '/studyboard',
    views: {
      'StudyBoard': {
        templateUrl: 'app/study-board/study-board.view.html',
        controller: 'StudyBoardController'
      }
    }
  })


  /**
   * Define the session nested state.
   * This is a good example for defining states nested
   * within tabs. The view name (StudyBoard@base.tab)
   * must reference the parent tab (StudyBoard) and the
   * parent's parent (@base.tab).
   */
  .state('base.tab.studyboard.session', {
    url: '/session/:id',
    views: {
      'StudyBoard@base.tab': {
        templateUrl: 'app/study-board/session/study-board-session.view.html',
        controller: 'StudyBoardSessionController'
      }
    }
  })

  .state('base.tab.studyboard.createSession', {
    url: '/createSession',
    views: {
      'StudyBoard@base.tab': {
        templateUrl: 'app/study-board/session/study-board-session-create-edit.view.html',
        controller: 'StudyBoardSessionCreateEditController'
      }
    }
  })

  .state('base.tab.studyboard.session.profile', {
    url: '/profile/:id',
    views: {
      'StudyBoard@base.tab': {
        templateUrl: 'app/profile/profile.view.html',
        controller: 'ProfileController'
      }
    },
    resolve: {
      isMyProfile: function() {
        return false;
      }
    }
  })

  .state('base.tab.studyboard.session.editSession', {
    url: '/editSession/:id',
    views: {
      'StudyBoard@base.tab': {
        templateUrl: 'app/study-board/session/study-board-session-create-edit.view.html',
        controller: 'StudyBoardSessionCreateEditController'
      }
    }
  })

  .state('base.tab.myteams', {
    url: '/myteams',
    views: {
      'MyTeams': {
        templateUrl: 'app/my-teams/my-teams.view.html',
        controller: 'MyTeamsController'
      }
    }
  })

  /**
   * Authentication Views
   */
  .state('auth', {
    abstract: true,
    url: '/auth',
    templateUrl: 'app/common/views/base-no-menu.abstract.html'
  })

  .state('auth.login', {
    url: '/login',
    templateUrl: 'app/auth/login/auth-login.view.html',
    controller: 'AuthLoginController'
  })

  .state('auth.register', {
    url: '/register',
    templateUrl: 'app/auth/register/auth-register.view.html',
    controller: 'AuthRegisterController'
  });

  /**
   * Define the default route.
   * This is used if the address entered matches none
   * of the defined routes.
   */
  $urlRouterProvider.otherwise('/tab/studyboard');
});
