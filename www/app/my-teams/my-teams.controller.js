/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function() {
  'use strict';

  studydraft.controller('MyTeamsController', ['$scope', 'mockDataService', MyTeamsController]);

  function MyTeamsController($scope, mockDataService) {

    mockDataService.getTeams().then(function(data) {
      $scope.myTeams = data;
    });
  }
})();
