/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function() {
  'use strict';

  studydraft.controller('SettingsController', ['$scope', SettingsController]);

  function SettingsController($scope) {
    $scope.foo = 'bar';
  }

})();
