(function() {
  'use strict';

  studydraft.directive('userItem', userItem);
  userItem.$inject = [];

  function userItem() {
     return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/common/directives/userItem/userItem.template.html',
      scope: {
        user: '='
      }
    };
  }

})();
