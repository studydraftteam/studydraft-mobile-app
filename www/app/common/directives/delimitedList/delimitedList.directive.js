(function() {
  'use strict';

  studydraft.directive('delimitedList', delimitedList);
  delimitedList.$inject = [];

  function delimitedList() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/common/directives/delimitedList/delimitedList.template.html',
      scope: {
        items: '=',
        itemClass: '=',
        property: '=',
        delimiter: '=',
        limitItems: '=',
        showRemainder: '='
      },
      link: function(scope) {

        scope.getValue = function(item) {
          if (scope.property !== undefined &&
            scope.property !== null) {
            return item[scope.property];
          } else {
            return item;
          }
        };

        scope.getRemainder = function() {
          if(scope.items) {
            return scope.items.length - scope.limitItems;
          } else {
            return 0;
          }
        };

      }
    };
  }

})();
