(function() {
  'use strict';

  studydraft.directive('studySessionItem', studySessionItem);
  studySessionItem.$inject = [];

  function studySessionItem() {
     return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/common/directives/studySessionItem/studySessionItem.template.html',
      scope: {
        studySession: '='
      }
    };
  }

})();
