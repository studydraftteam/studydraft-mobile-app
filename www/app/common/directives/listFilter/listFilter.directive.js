(function() {
  'use strict';

  studydraft.directive('listFilter', listFilter);
  listFilter.$inject = [];

  function listFilter() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/common/directives/listFilter/listFilter.template.html',
      scope: {
        filterText: '=',
        placeholder: '='
      },
      link: function(/* scope, element, attrs */) {
        // Define functions here
      }
    };
  }

})();
