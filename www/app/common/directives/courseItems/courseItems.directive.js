(function() {
  'use strict';

  function courseCard () {
      // function link(scope, element, attrs) {
      //   console.log('courseItems');
      // }

      return {
        // link: link,
        restrict: 'E',
        replace: true,
        templateUrl: 'app/common/directives/courseItems/courseItems.template.html',
        scope: {
          courses: '='
        }
      };
  }

  studydraft.directive('courseItems', courseCard);
  courseCard.$inject = [];

})();
