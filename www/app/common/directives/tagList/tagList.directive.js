(function() {
  'use strict';

  studydraft.directive('tagList', tagList);
  tagList.$inject = [];

  function tagList() {
     return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/common/directives/tagList/tagList.template.html',
      scope: {
        items: '=',
        itemClass: '=',
        property: '=',
        class: '=',
        canCreate: '=',
        onCreate: '&',
        canDelete: '=',
        onDelete: '&',
        onClick: '&'
      },
      link: function(scope, element, attrs) {
        scope.getValue = function(item) {
          if(scope.property !== undefined
            && scope.property !== null) {
              return item[scope.property];
          } else {
            return item;
          }

        }
      }
    };
  }

})();
