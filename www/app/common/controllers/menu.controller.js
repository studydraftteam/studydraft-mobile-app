/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function(){
    'use strict';

    studydraft.controller('MenuController', ['AuthService', '$scope', '$window', MenuController]);

    function MenuController(AuthService, $scope, $window) {

      $scope.logout = logout;
      $scope.auth = AuthService;

      function logout() {
        AuthService.logout()
          .then(function() {
            $window.location.reload();
          });
      }

    }

})();
