(function() {
  'use strict';

  studydraft.service('StudySessionService', StudySessionService);

  StudySessionService.$inject = ['AuthService', 'config', '$http', '$log', '$q'];

  function StudySessionService(AuthService, config, $http, $log, $q) {
    var apiURL = config.API_URL + '/studySession';

    var service = {
      getStudySession: getStudySession,
      createStudySession: createStudySession,
      saveStudySession: saveStudySession,
      deleteStudySession: deleteStudySession,
      getNewStudySession: getNewStudySession,
      getActiveStudySessionList: getActiveStudySessionList,
      getStudySessionList: getStudySessionList,
      joinRequestSubmit: joinRequestSubmit,
      associate: associate,
      disassociate: disassociate
    };

    return service;

    function getStudySession(id, populate) {
      return $http.get(apiURL + '/' + id + '?populate=[' + populate + ']')
        .then(function(response) {
          return response.data;
        });
    }

    function createStudySession(studySession) {
      return $http.post(apiURL, studySession)
        .then(function(response) {
          return response.data;
        });
    }

    function saveStudySession(studySession) {
      return $http.post(apiURL + '/' + studySession.id, studySession)
        .then(function(response) {
          return response.data;
        });
    }

    function getActiveStudySessionList(university) {
      return AuthService
        .checkUser()
        .then(function() {
          return $http.get(apiURL + '/active' + '?university=' + university + '&populate=[users,courses]')
            .then(function(response) {
              return response.data;
            });
        });
    }

    function getStudySessionList() {
      return $http.get(apiURL).then(function(response) {
        return response.data;
      });
    }

    function joinRequestSubmit(id) {
      return $http.post(apiURL + '/' + id + '/joinrequest/submit')
        .then(function(response) {
          return response.data;
        })
    }

    function getNewStudySession() {
      return $q(function(resolve) {
        var baseSession = {
          description: '',
          endingAt: new Date(),
          tags: [],
          locationDescription: '',
          locationGeo: [],
          maxUsers: 0,
          university: AuthService.currentUser.university,
          courses: [],
          users: []
        };

        resolve(baseSession);
      });
    }

    function associate(id, property, propertyIds) {
      return $http.post(apiURL + '/' + id + '/associate', {
        property: property,
        propertyIds: propertyIds
      });
    }

    function disassociate(id, property, propertyIds) {
      return $http.post(apiURL + '/' + id + '/disassociate', {
        property: property,
        propertyIds: propertyIds
      });
    }

    function deleteStudySession(id) {
      return $http.delete(apiURL + '/' + id);
    }

  }
})();
