(function() {
  'use strict';

  studydraft.service('PushNotificationService', PushNotificationService);

  PushNotificationService.$inject = ['config', '$cordovaPush', '$http', '$ionicPlatform'];

  function PushNotificationService(config, $cordovaPush, $http, $ionicPlatform) {

    var apiURL = config.API_URL + '/device';

    var self = this;
    self.pushConfig = {};
    self.platform = ionic.Platform.platform();
    self.device = ionic.Platform.device();
    self.currentUser = null;
    self.deviceId = null;
    self.androidConfig = {
      'senderID': config.GOOGLE_PROJECT_NUMBER
    };
    self.iosConfig = {
      'badge': 'true',
      'sound': 'true',
      'alert': 'true'
    };

    var service = {
      handleNotification: handleNotification,
      initializeDevice: initializeDevice,
      registerDevice: registerDevice
    };

    return service;

    function initializeDevice(user) {
      self.currentUser = user;
      // Determine device platform and set config
      switch (self.platform) {
        case 'android':
          self.pushConfig = self.androidConfig;
          return $cordovaPush.register(self.pushConfig);
          break;
        case 'ios':
          self.pushConfig = self.iosConfig;
          return $cordovaPush.register(self.pushConfig)
            .then(function(deviceId) {
              self.deviceId = deviceId;
            })
            .then(registerDevice);
          break;
      }
    }

    function handleNotification(event, data) {
      switch (data.event) {
        case 'registered':
          console.log('handleNotification - registered', data);
          self.deviceId = data.regid;
          registerDevice();
          break;
        default:
          alert(JSON.stringify(data));
          console.log(data);
      }
    }

    function registerDevice() {

      console.log('registerDevice', self.currentUser, self.platform, self.deviceId);

      return $http.post(apiURL + '/register', {
          owner: self.currentUser,
          platform: self.platform,
          deviceId: self.deviceId
        })
        .then(function(response) {
          console.log(response);
        });

    }
  }
})();
