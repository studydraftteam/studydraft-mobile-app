(function() {
  'use strict';

  studydraft.service('UserService', UserService);

  UserService.$inject = ['AuthService', 'config', '$cordovaOauth', '$http', '$log'];

  function UserService(AuthService, config, $cordovaOauth, $http, $log) {
    var apiURL = config.API_URL + '/user/';

    var service = {
      getUser: getUser,
      saveUser: saveUser,
      searchUsers: searchUsers,
      getCourses: getCourses,
      resetPassword: resetPassword,
      changePassword: changePassword,
      associate: associate,
      disassociate: disassociate,
      syncFacebook: syncFacebook
    };

    return service;

    function getUser(id, populate) {
      return $http.get(apiURL + id + '?populate=[' + populate + ']')
        .then(function(response) {
          return response.data;
        }, function(error) {
          $log.debug(error);
        });
    }

    function saveUser(user) {
      return $http.put(apiURL + user.id, user)
        .then(function(response) {
          return response.data;
        }, function(error) {
          $log.debug(error);
        });
    }

    function searchUsers(searchText, universityFixedId) {
      return $http.get(apiURL + 'search?name=' + searchText + '&?university=' + universityFixedId)
        .then(function(response) {
          return response.data;
        }, function(error) {
          $log.debug(error);
        });
    }

    function getCourses(id) {
      return $http.get(apiURL + id + '?populate=[courses]')
        .then(function(response) {
          return response.data.courses;
        });
    }

    function resetPassword(email) {
      return $http.post(apiURL + 'resetpassword', {
        email: email
      });
    }

    function changePassword(id, currentPassword, newPassword, newPasswordConfirm) {
      return $http.post(apiURL + id + '/changePassword', {
        currentPassword: currentPassword,
        newPassword: newPassword,
        newPasswordConfirm: newPasswordConfirm
      });
    }

    /**
     * Associate the user with another model defined
     * as the passed property and using the propertyIds.
     * @param  {Number} id          The user's id
     * @param  {String} property    Name of the property at which the association lives.
     * @param  {String} propertyIds Comma-separated string of ids to which the user will be associated.
     * @return {Promise}            $http promise
     */
    function associate(id, property, propertyIds) {
      return $http.post(apiURL + id + '/associate', {
          property: property,
          propertyIds: propertyIds
        });
    }

    function disassociate(id, property, propertyIds) {
      return $http.post(apiURL + id + '/disassociate', {
        property: property,
        propertyIds: propertyIds
      });
    }

    function syncFacebook(id) {
      return $cordovaOauth.facebook(config.FB_APP_ID, config.FB_PERMISSIONS)
        .then(function(fbResponse) {
          return $http.post(apiURL + id + '/syncfacebook', {
              accessToken: fbResponse.access_token
            })
            .success(function(apiResponse) {
              AuthService.currentUser = apiResponse;
              self.user = apiResponse;
              return apiResponse;
            });
        });
    }
  }
})();
