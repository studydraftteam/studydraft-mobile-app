/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/10/15
 *
 * This factory is responsible for maintaining the current user
 * and user state for the application.
 *
 */
(function() {
  'use strict';

  studydraft.service('AuthService', ['config', '$cordovaOauth', '$http', '$ionicPopup',
    'localStorageService', 'PushNotificationService', '$q', AuthService
  ]);

  function AuthService(config, $cordovaOauth, $http, $ionicPopup, localStorageService, PushNotificationService, $q) {

    var self = this,
      apiURL = config.API_URL + '/auth';

    self.checkUser = checkUser;
    self.login = login;
    self.register = register;
    self.logout = logout;
    self.loginFacebook = loginFacebook;
    self.registerFacebook = registerFacebook;

    function login(credentials) { // name, username, password, email
      return $http.post(apiURL + '/login', credentials)
        .success(function(apiResponse) {
          initializeSession(apiResponse);
        });
    }

    function register(credentials) { // name, username, password, email
      return $http.post(apiURL + '/register', credentials);
    }

    function logout() {
      var dfr = $q.defer();

      self.currentUser = undefined;
      self.sdAccessToken = undefined;
      localStorageService.clearAll();

      dfr.resolve({});

      return dfr.promise;
    }

    function checkUser() {
      var dfr = $q.defer();
      // Are the user and access token set?
      if (self.currentUser && self.sdAccessToken) {
        dfr.resolve(self.currentUser);
      }
      // Can we retrieve the user via the access token?
      else if (localStorageService.get('sdaccesstoken')) {
        // Get the access token from local storage
        self.sdAccessToken = localStorageService.get('sdaccesstoken');
        // Set a header token to be used on all http requests
        $http.defaults.headers.common['sdaccesstoken'] = self.sdAccessToken;
        // Get the current user
        $http.get(config.API_URL + '/user/me')
          .success(function(user) {
            self.currentUser = user;
            dfr.resolve(user);
          })
          .error(function(error) {
            self.logout();
            dfr.reject(error);
          });
      }
      // No, reject
      else {
        self.logout();
        dfr.reject('checkUser failed');
      }
      return dfr.promise;
    }

    function registerFacebook() {
      return $cordovaOauth.facebook(config.FB_APP_ID, config.FB_PERMISSIONS)
        .then(function(fbResponse) {
          return $http.post(apiURL + '/register/facebook', {
              accessToken: fbResponse.access_token
            })
            .success(function(apiResponse) {
              initializeSession(apiResponse);
              return apiResponse;
            });
        });
    }

    function loginFacebook() {
      return $cordovaOauth.facebook(config.FB_APP_ID, config.FB_PERMISSIONS)
        .then(function(fbResponse) {
          return $http.post(apiURL + '/login/facebook', {
              accessToken: fbResponse.access_token
            })
            .success(function(apiResponse) {
              initializeSession(apiResponse);
              return apiResponse;
            });
        });
    }

    function initializeSession(apiResponse) {
      self.currentUser = apiResponse.user;
      self.sdAccessToken = apiResponse.sdaccesstoken;
      $http.defaults.headers.common['sdaccesstoken'] = self.sdAccessToken;
      localStorageService.set('sdaccesstoken', self.sdAccessToken);
      PushNotificationService.initializeDevice(self.currentUser);
    }

    return self;
  }

})();
