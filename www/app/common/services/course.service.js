(function() {
  'use strict';

  studydraft.service('CourseService', CourseService);

  CourseService.$inject = ['config', '$http'];

  function CourseService(config, $http) {
    var apiURL = config.API_URL + '/course';

    var service = {
      getCourse: getCourse,
      getCourseList: getCourseList,
      createCourse: createCourse
    };

    return service;

    function getCourse(id, populate) {
      return $http.get(apiURL + '/' + id + '?populate=[' + populate + ']')
        .then(function(response) {
          return response.data;
        });
    }

    function getCourseList() {
      return $http.get(apiURL).then(function(response) {
        return response.data;
      });
    }

    function createCourse(course) {
      return $http.post(apiURL, course);
    }
  }
})();
