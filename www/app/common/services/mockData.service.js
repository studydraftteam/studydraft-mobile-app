(function() {
    'use strict';

    /**
     * Alex Klibisz, aklibisz@utk.edu
     *
     * 6/10/15
     *
     * This service is responsible for storing mock data to use when real data is
     * not available
     *
     * NOTE:
     *
     * TODO:
     */

    studydraft.service('mockDataService', ['$http', mockDataService]);

    function mockDataService($http) {

        var service = {
            getCourse: getCourse,
            getUniversity: getUniversity,
            getUser: getUser,
            getCourses: getCourses,
            getUniversities: getUniversities,
            getUsers: getUsers,
            getBoard: getBoard,
            getPost: getPost,
            getTeams: getTeams
        };

        return service;

        function getCourse(index) {
            return $http.get('app/mock/courses.json').then(function(result) {
                return result.data.Courses[index];
            }, function(err) {
                $log.error(err);
            });
        }

        function getUniversity(index) {
            return $http.get('app/mock/universities.json').then(function(result) {
                return result.data.Users[index];
            }, function(err) {
                $log.error(err);
            });
        }

        function getUser(index) {
            return $http.get('app/mock/users.json').then(function(result) {
                return result.data.Users[index];
            }, function(err) {
                $log.error(err);
            });
        }

        function getCourses() {
            return $http.get('app/mock/courses.json').then(function(result) {
                return result.data.Courses;
            }, function(err) {
                $log.error(err);
            });
        }

        function getUniversities() {
            return $http.get('app/mock/universities.json').then(function(result) {
                return result.data.Users;
            }, function(err) {
                $log.error(err);
            });
        }

        function getUsers() {
            return $http.get('app/mock/users.json').then(function(result) {
                return result.data.Users;
            }, function(err) {
                $log.error(err);
            });
        }

        function getBoard() {
            return $http.get('app/mock/board.json').then(function(result) {
                return result.data.Board;
            }, function(err) {
                $log.error(err);
            });
        }

        function getPost() {
            return $http.get('app/mock/post.json').then(function(result) {
                return result.data.Post;
            }, function(err) {
                $log.error(err);
            });
        }

        function getTeams() {
            return $http.get('app/mock/teams.json').then(function(result) {
                return result.data.Teams;
            }, function(err) {
                $log.error(err);
            });
        }
    }

})();
