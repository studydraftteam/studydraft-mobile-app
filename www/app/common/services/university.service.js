(function() {
    'use strict';

    studydraft.service('UniversityService', UniversityService);

    UniversityService.$inject = ['config', '$http', '$log'];

    function UniversityService(config, $http, $log) {
        var apiURL = config.API_URL + '/university';

        var service = {
            getUniversity: getUniversity,
            getUniversityList: getUniversityList,
            getCourses: getCourses
        };

        return service;

        function getUniversity(id) {
            return $http.get(apiURL + '/' + id).then(function(response) {
                return response.data;
            }, function (error) {
                $log.debug(error);
            });
        }

        function getUniversityList() {
            return $http.get(apiURL + '?limit=-1').then(function(response) {
                return response.data;
            }, function (error) {
                $log.debug(error);
            });
        }

        function getCourses(id) {
            return $http.get(apiURL + '/' + id +
                    '?populate=[courses]')
                .then(function(response) {
                    return response.data.courses;
                }, function(error) {
                    $log.debug(error);
                });
        }
    }
})();
