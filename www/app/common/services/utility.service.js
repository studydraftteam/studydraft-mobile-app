(function() {
  'use strict';

  studydraft.service('utilityService', utilityService);

  utilityService.$inject = ['$log'];

  function utilityService($log) {
    var service = {
      hasData: hasData
    };

    return service;

    function hasData(item) {
      if (angular.isUndefined(item)) return false;

      if (item === null) return false;

      if (angular.isArray(item)) {
        return item.length > 0;
      }

      if (angular.isObject(item)) {
        return Object.keys(item).length === 0;
      }

      if(angular.isString(item)) {
        return (item !== '');
      }

      return true;
    }
  }

})();
