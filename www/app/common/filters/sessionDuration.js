(function() {
  'use strict';

  studydraft.filter('sessionDuration', sessionDuration);

  sessionDuration.$inject = [];

  function sessionDuration() {
    return function(start, end) {
        var diff = (moment(end).from(start, true));
        return diff + ' left';
    };
  }

})();
