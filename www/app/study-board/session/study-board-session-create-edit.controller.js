(function() {
  'use strict';

  studydraft.controller('StudyBoardSessionCreateEditController', ['StudySessionService', 'UserService', 'AuthService', 'UniversityService', 'utilityService', '$ionicLoading', '$ionicPopover', '$ionicPopup', '$scope', '$stateParams', '$timeout', '$ionicModal', '$log', '$state', '$rootScope', StudyBoardSessionCreateEditController]);

  function StudyBoardSessionCreateEditController(StudySessionService, UserService, AuthService, UniversityService, utilityService, $ionicLoading, $ionicPopover, $ionicPopup, $scope, $stateParams, $timeout, $ionicModal, $log, $state, $rootScope) {

    $scope.studySession = {};
    $scope.viewTitle = '';
    $scope.isEditing = false;
    $scope.isCreating = false;
    $scope.deleteUser = deleteUser;
    $scope.deleteCourse = deleteCourse;
    $scope.deleteTag = deleteTag;
    $scope.submitCreate = submitCreate;
    $scope.submitSave = submitSave;

    /* Time Related Varaibles */
    $scope.start = {
      time: '',
      date: {}
    };
    $scope.sessionTimes = [];
    $scope.durationOptions = [];

    function setTimeProperties() {
      var sessionTime = moment($scope.studySession.startingAt);

      $scope.start = {
        time: {
          display: sessionTime.format('h:mm A'),
          value: sessionTime
        },
        date: sessionTime.toDate()
      };
    }

    /* Create Time Options for Duration Select */
    var current = moment();
    var remainder = (30 - current.minute()) % 30;
    var startLoop = current.add(remainder, 'minutes');
    var startDisplay = startLoop.format('h:mm A');

    $scope.start.date = current.toDate();
    $scope.start.time = {
      display: 'Now',
      value: current
    };

    $scope.sessionTimes.push($scope.start.time);
    $scope.sessionTimes.push({
      display: startDisplay,
      value: startLoop
    });

    var timeSeg = startLoop.add(30, 'minutes');
    var timeSegDisplay = timeSeg.format('h:mm A');

    while (startDisplay !== timeSegDisplay) {
      $scope.sessionTimes.push({
        display: timeSegDisplay,
        value: timeSeg
      });

      timeSeg = timeSeg.add(30, 'minutes');
      timeSegDisplay = timeSeg.format('h:mm A');

      if ($scope.sessionTimes.length > 48) break;
    }

    /* Create Duration List */
    $scope.durationOptions.push({
      value: 1,
      display: '1 Hour'
    });

    for (var i = 2; i < 24; i++) {
      $scope.durationOptions.push({
        value: i,
        display: i + ' Hours'
      });
    }

    $scope.duration = $scope.durationOptions[0];


    /* Get Study Session by ID */
    if ($stateParams.id) {
      $scope.viewTitle = 'Edit';
      $scope.isEditing = true;
      StudySessionService.getStudySession($stateParams.id, 'users,courses')
        .then(function(response) {
          $scope.studySession = response;
          setTimeProperties();
        });
    } else {
      $scope.viewTitle = 'Create Study Session';
      $scope.isCreating = true;
      /* Get a Base Study Session Object */
      StudySessionService.getNewStudySession()
        .then(function(response) {
          $scope.studySession = response;
          return $scope.studySession;
        })
        .then(function(studySession) {
          AuthService.checkUser()
            .then(function(currentUser) {
              studySession.users.push(currentUser);
              studySession.createdBy = currentUser;
            });
        })
        .catch(function(error) {
          $log.error(error);
        });
    }

    function processTime() {
      var startTime = moment($scope.start.time.date);
      startTime = startTime.set($scope.start.time.value.hour(), 'hour');
      startTime = startTime.set($scope.start.time.value.minute(), 'minute');

      // $log.debug(startTime.format('lll'));
      // $log.debug(startTime.add($scope.duration.value, 'hours').format('lll'));

      $scope.studySession.startingAt = startTime.toDate();
      $scope.studySession.endingAt = startTime.add($scope.duration.value, 'hours').toDate();
    }

    function isValidSession() {
      // has at least one member
      console.log($scope.studySession);
      if (!utilityService.hasData($scope.studySession.users)) return false;

      // has location
      if (!utilityService.hasData($scope.studySession.locationDescription)) return false;

      return true;
    }

    /* Save or create new Study Session ------------------------------- */
    function submitSave() {
      processTime();
      if (isValidSession()) {
        StudySessionService.saveStudySession($scope.studySession)
          .then(function() {
            $ionicPopup.alert({
              title: 'Changes saved'
            });
            // $location.path('/tab/studyboard/session/' + $scope.studySession.id);
            $state.go('base.tab.studyboard.session', {
                id: $scope.studySession.id
              })
              .then(function() {
                $rootScope.$broadcast('CURRENT_STUDY_SESSION', {
                  reload: true
                });
              });
          });
      } else {
        $ionicPopup.alert({
          title: 'Study Session is invalid',
          template: '<center>' + 'some error message' + '</center>'
        });
      }
    }

    function submitCreate() {
      processTime();
      if (isValidSession()) {
        StudySessionService.createStudySession($scope.studySession)
          .then(function(response) {
            $ionicPopup.alert({
              title: 'Session Created'
            });
            // $location.path('/tab/studyboard');
            $state.go('base.tab.studyboard')
              .then(function() {
                $rootScope.$broadcast('CURRENT_STUDY_BOARD', {
                  reload: true
                });
              });
          }, function(response) {
            $log.error(response);
            $ionicPopup.alert({
              title: 'Error Saving Session',
              template: '<center>' + response.data.summary + '</center>'
            });
          });
      } else {
        $ionicPopup.alert({
          title: 'Study Session is invalid',
          template: '<center>' + 'some error message' + '</center>'
        });
      }
    }

    /* Create the modals ------------------------------- */
    // Course Modal
    $ionicModal.fromTemplateUrl('app/study-board/session/courses.modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
      init: function() {
        $scope.addCoursesForm = new AddCoursesForm();
      }
    }).then(function(modal) {
      $scope.addCoursesModal = modal;
    });

    // Members Modal
    $ionicModal.fromTemplateUrl('app/study-board/session/members.modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
      init: function() {
        $scope.addMembersForm = new AddMembersForm();
      }
    }).then(function(modal) {
      $scope.addMembersModal = modal;
    });

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.addCoursesModal.remove();
      $scope.addMembersModal.remove();
    });

    function deleteUser(index) {
      $scope.studySession.users.splice(index, 1);
    }

    function deleteCourse(index) {
      $scope.studySession.courses.splice(index, 1);
    }

    function deleteTag(index) {
      $scope.studySession.tags.splice(index, 1);
    }

    function AddCoursesForm() {
      var self = this;
      self.submit = submit;
      $ionicLoading.show({
        template: 'Loading...'
      });
      // First, get only the User's courses
      UserService.getCourses(AuthService.currentUser.id)
        .then(function(courses) {
          self.userCourses = courses;

          angular.forEach(self.userCourses, function(course) {
            self.checkIfSelected(course);
          });

          // Get courses at the user's university
          UniversityService.getCourses(AuthService.currentUser.university)
            .then(function(courses) {
              self.allCourses = courses;

              // I know this isn't efficient but it'll do for now
              angular.forEach(self.allCourses, function(course) {
                self.checkIfSelected(course);
              });

              angular.forEach(self.allCourses, function(course, index) {
                self.removeUserCourses(course, index);
              });

              $ionicLoading.hide();
            });
        });

      self.checkIfSelected = function(currentCourse) {
        angular.forEach($scope.studySession.courses, function(course) {
          if (currentCourse.id === course.id) {
            currentCourse.selected = true;
          }
        });
      };

      self.removeUserCourses = function(currentCourse, index) {
        angular.forEach(self.userCourses, function(userCourse) {
          if (currentCourse.id === userCourse.id) {
            self.allCourses.splice(index, 1);
          }
        });
      };

      function submit() {
        var newCourses = [];

        self.userCourses.forEach(function(course) {
          if (course.selected) {
            newCourses.push(course);
          }
        });
        self.allCourses.forEach(function(course) {
          if (course.selected) {
            newCourses.push(course);
          }
        });

        $scope.studySession.courses = newCourses;
      }
    }

    function AddMembersForm() {
      var self = this;
      self.searchText = '';
      self.members = [];

      self.search = function() {
        if (angular.isDefined(self.searchText) && self.searchText !== '') {
          $ionicLoading.show({
            template: 'Loading...'
          });

          UserService.searchUsers(self.searchText, AuthService.currentUser.university)
            .then(function(data) {
              self.members = data;
              $ionicLoading.hide();
            });
        }
      };

      self.addMember = function(member, $index) {
        var isAMember = false;
        $scope.studySession.users.forEach(function(user) {
          if (user.id === member.id) {
            isAMember = true;
          }
        });
        if (!isAMember) {
          $scope.studySession.users.push(member);
          self.members.splice($index, 1);
          $ionicLoading.show({
            template: 'Member added'
          });
          $timeout($ionicLoading.hide, 1000);
        } else {
          $ionicLoading.show({
            template: 'Already a member'
          });
          $timeout($ionicLoading.hide, 1000);
        }
      };
    }


    /* Popups ------------------------------- */
    // Tags Popup
    $scope.addTags = function() {
      $scope.data = {};

      var tagPopup = $ionicPopup.show({
        template: "<input type='text' ng-model='data.tags'>",
        title: 'Enter New Tag',
        subTitle: 'Separate tags with a comma',
        scope: $scope,
        buttons: [{
          text: 'Cancel'
        }, {
          text: '<b>Add Tags</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.data.tags) {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {
              // remove spaces after commas
              $scope.data.tags = $scope.data.tags.replace(/,\s/g, ',');

              // parse string into array
              var array = $scope.data.tags.split(',');

              // prefix with # if not present and check for existing tags
              angular.forEach(array, function(tag, index) {
                if (tag.charAt(0) !== '#') tag = '#' + tag;

                angular.forEach($scope.studySession.tags, function(existingTag, existingIndex) {
                  if (tag === existingTag) $scope.studySession.tags.splice(existingIndex, 1);
                });

                $scope.studySession.tags.push(tag);
              });
            }
          }
        }]
      });
    };

    // Start Time Popup
    $scope.editStartTime = function() {
      var tagPopup = $ionicPopup.show({
        templateUrl: 'app/study-board/session/editStartPopup.html',
        title: 'Editing Session Start',
        subTitle: 'When will you be studying?',
        scope: $scope,
        buttons: [{
          text: 'Cancel',
          onTap: function() {}
        }, {
          text: '<b>Save</b>',
          type: 'button-positive',
          onTap: function() {
            $log.log($scope.start.time, $scope.start.date);
          }
        }]
      });
    };
  }

})();
