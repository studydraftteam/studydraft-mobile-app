(function() {
  'use strict';

  studydraft.controller('StudyBoardSessionController', ['AuthService', '$ionicPopover', '$ionicPopup', '$location', 'StudySessionService', '$scope', '$state', '$stateParams', StudyBoardSessionController]);

  function StudyBoardSessionController(AuthService, $ionicPopover, $ionicPopup, $location, StudySessionService, $scope, $state, $stateParams) {
    var sessionId = $stateParams.id;
    $scope.studySession = {};
    $scope.joinSession = joinSession;
    $scope.leaveSession = leaveSession;
    $scope.deleteSession = deleteSession;
    $scope.activeTab = 'members';
    $scope.isSessionMember = false;
    $scope.isSessionCreator = false;

    $scope.$on('CURRENT_STUDY_SESSION', function(event, params) {
      if(params.reload) {
        controllerInit();
      }
    });

    function controllerInit() {
      // Get the Study Session
      StudySessionService.getStudySession(sessionId, 'users,courses,createdBy')
        .then(function(response) {
          $scope.studySession = response;
          return $scope.studySession;
        })
        .then(function(studySession) {
          // Check if the current user is a session member, session owner
          AuthService.checkUser()
            .then(function(currentUser) {
              studySession.users.forEach(function(user) {
                if(user.id === currentUser.id) {
                  $scope.isSessionMember = true;
                }
              });
              if(studySession.createdBy.id === currentUser.id) {
                $scope.isSessionCreator = true;
              }
            });
        })
        .catch(function(error) {
          $log.error(error);
        });
    }
    controllerInit();

    $ionicPopover.fromTemplateUrl('options-popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.optionsPopover = popover;
    });

    function joinSession() {
      $ionicPopup.confirm({
        title: 'Join Session?',
        template: 'Click OK to send a request to the session members'
      })
      .then(function(yes) {
        if(yes) {
          StudySessionService.joinRequestSubmit($scope.studySession.id)
            .then(function() {
              $ionicPopup.alert({
                title: 'Request submitted',
                template: '<center>Your request has been submitted!</center>'
              });
            });
        }
      });
    }

    function leaveSession() {
      StudySessionService.disassociate($scope.studySession.id, 'users', AuthService.currentUser.id)
        .then(function() {
          $scope.optionsPopover.hide();
          $location.path('/tab/studyboard');
        })
        .catch(function(error) {
          $ionicPopup.alert({
            title: 'Something went wrong'
          });
        });
    }

    function deleteSession() {
      StudySessionService.deleteStudySession($scope.studySession.id)
        .then(function() {
          $scope.optionsPopover.hide();
          $location.path('/tab/studyboard');
        });
    }

  }
})();
