/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 6/9/15
 */
(function() {
  'use strict';

  studydraft.controller('StudyBoardController', ['AuthService', 'UserService', 'StudySessionService', '$ionicModal', '$ionicLoading', '$ionicPopup', '$log', '$rootScope', '$scope', StudyBoardController]);

  function StudyBoardController(AuthService, UserService, StudySessionService, $ionicModal, $ionicLoading, $ionicPopup, $log, $rootScope, $scope) {
    $scope.studySessions = [];
    $scope.getStudySessions = getStudySessions;
    $scope.getStudySessions();

    $scope.$on('CURRENT_STUDY_BOARD', function(event, params) {
      if(params.reload) {
        $scope.getStudySessions();
      }
    });

    function getStudySessions() {
      AuthService.checkUser()
        .then(function(user) {
          $ionicLoading.show({
            template: 'Loading Study Sessions'
          });
          StudySessionService.getActiveStudySessionList(user.university)
            .then(function(response) {
              $scope.studySessions = response;
              $scope.$broadcast('scroll.refreshComplete');
              $ionicLoading.hide();
            })
            .catch(function(error) {
              $ionicPopup.show({
                title: 'Something went wrong',
                template: '<center>' + error.summary + '</center>'
              });
            });
        });
    }
  }
})();
