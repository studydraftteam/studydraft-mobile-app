var appDependencies = [
  'ionic', // The app structure, styling, etc.
  'LocalStorageModule', // Local Storage
  'ngCordova', // Cordova utilities wrapped for angular
  'ionic-timepicker' // Ionic Timepicker --> https://github.com/rajeshwarpatlolla/ionic-timepicker
];

// Add dependencies that change based on environment
if(!window.cordova) {
  appDependencies.push('ngCordovaMocks');
}

var studydraft = angular.module('studydraft', appDependencies);

/**
 * Default Cordova Setup
 */
studydraft.run(function($ionicPlatform) {
  'use strict';
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

/**
 * Push Notification Setup
 *
 * Attaches a notification handler and fires the device registration.
 */
studydraft.run(function($ionicPlatform, PushNotificationService, $rootScope) {
  'use strict';
  $ionicPlatform.ready(function() {
    $rootScope.$on('$cordovaPush:notificationReceived', PushNotificationService.handleNotification);
    // PushNotificationService.registerDevice();
  });
});

/**
 * Authentication Setup
 *
 * This establishes a listener that is fired every time a user
 * changes state in the application to verify authentication
 * against the Parse backend.
 */
studydraft.run(['$location', '$state', '$rootScope', 'AuthService', function($location, $state, $rootScope, AuthService) {
  'use strict';
  $rootScope.$on('$stateChangeStart', function() {

    AuthService.checkUser()
      .catch(function() {
        $location.path('/auth/login');
      });

  });
}]);
